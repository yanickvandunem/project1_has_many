json.array!(@appointments) do |appointment|
  json.extract! appointment, :id, :patient_id, :specialist_id, :complaint, :appointment_date, :fee
  json.url appointment_url(appointment, format: :json)
end
