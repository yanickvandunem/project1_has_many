# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

#Insurance.create (name: '', street_address: '')
Insurance.create(name: 'Red Cross America',street_address: '432 JFK Blvd.')
Insurance.create(name: 'AIG LLC.', street_address: '3838 Smith St.')
Insurance.create(name: 'Blue Cross Texas', street_address: '2938 Lakes Ln')
Insurance.create(name: 'HTH International', street_address: '38222 Luanda Ave')
Insurance.create(name: 'Gainsco LLC.', street_address: '77899 Houston Brooks Ln')


#Patient.create (name: '', street_address: '')
Patient.create(name: 'Jesus Corunha', street_address: '3333 West ave', insurance_id: 3)
Patient.create(name: 'Mateus Diablo', street_address: '2234 Richmond', insurance_id: 3)
Patient.create(name: 'Barack Obama', street_address: '8769 Westheimer', insurance_id: 1)
Patient.create(name: 'Hilary Clinton', street_address: '3889 Cullen', insurance_id: 2)
Patient.create(name: 'Jeff Bezzos', street_address: '22399 Calhoun', insurance_id: 2)



#Specialist.create (name: '', street_address: '')
Specialist.create(name: 'Adam Smith', speciality: 'General Clinic')
Specialist.create(name: 'Gordon Brown', speciality: 'Dentist')
Specialist.create(name: 'Jake Cameroon', speciality: 'Psychologist')
Specialist.create(name: 'Jonas Savimbi', speciality: 'Orthopedist')
Specialist.create(name: 'Jose Eduardo', speciality: 'Dentist')



#Appointment.create (patient_id: ,specialist_id: , complaint: '', appointment_date: , fee: )
Appointment.create(patient_id: 3,specialist_id: 3, complaint: 'Headache', appointment_date:'2013-03-02', fee: 262)
Appointment.create(patient_id: 4,specialist_id: 2, complaint: 'Insomnia', appointment_date:'2014-10-12', fee: 939)
Appointment.create(patient_id: 5,specialist_id: 5, complaint: 'PTSD Symptoms', appointment_date:'2013-09-11', fee: 377)
Appointment.create(patient_id: 5,specialist_id: 1, complaint: 'Fever', appointment_date:'2014-09-12', fee: 55.7)
Appointment.create(patient_id: 1,specialist_id: 4, complaint: 'Skin burn', appointment_date:'2014-04-07', fee: 538)
Appointment.create(patient_id: 3,specialist_id: 2, complaint: 'Hair Loss', appointment_date:'2015-01-01', fee: 278)
Appointment.create(patient_id: 2,specialist_id: 3, complaint: 'Depression', appointment_date:'2014-12-04', fee: 3000)